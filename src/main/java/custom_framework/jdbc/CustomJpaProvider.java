package custom_framework.jdbc;

import com.models.Company;
import custom_framework.annotations.CustomColumn;
import custom_framework.annotations.CustomEntity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class CustomJpaProvider {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "postgres", "postgres");

        Class entityClass = Company.class;
        List<FieldToAttributeTableEntry> fieldToAttributeTableEntries = new ArrayList<>();


        Annotation annotation = entityClass.getAnnotation(CustomEntity.class);
        if (annotation != null) {
            String tableName = ((CustomEntity) annotation).name();


            StringJoiner columnNames = new StringJoiner(",");

            System.out.println(columnNames.toString());
            Field[] declaredFields = entityClass.getDeclaredFields();
            for (Field field : declaredFields) {
                CustomColumn fieldAnnotation = field.getAnnotation(CustomColumn.class);
                if (fieldAnnotation != null) {
                    columnNames.add(fieldAnnotation.name());
                    FieldToAttributeTableEntry attributeTableEntry = new FieldToAttributeTableEntry(field.getName(), fieldAnnotation.name());
                    fieldToAttributeTableEntries.add(attributeTableEntry);

                }
            }

            System.out.println(tableName);
            System.out.println(columnNames);
            System.out.println(fieldToAttributeTableEntries);


            String sql = String.format("select %s from %s", columnNames, tableName);
            System.out.println(sql);


            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            List resultList = new ArrayList();
            while (resultSet.next()) {
                Object newInstance = entityClass.newInstance();
                for (FieldToAttributeTableEntry fieldToAttributeTableEntry : fieldToAttributeTableEntries) {
                    String attributeValue = resultSet.getString(fieldToAttributeTableEntry.getAttributeTableName());

                    Field declaredField = newInstance.getClass().getDeclaredField(fieldToAttributeTableEntry.getClassFieldName());
                    declaredField.setAccessible(true);
                    declaredField.set(newInstance, attributeValue);
                }
                resultList.add(newInstance);
            }

            resultList.stream().forEach(model -> System.out.println(model));
        }
    }


}

/**
 * field from class -> attribute from table
 */
class FieldToAttributeTableEntry {
    private String classFieldName;//sourceId
    private String attributeTableName;//source_id


    public FieldToAttributeTableEntry(String classFieldName, String attributeTableName) {
        this.classFieldName = classFieldName;
        this.attributeTableName = attributeTableName;
    }

    public String getClassFieldName() {
        return classFieldName;
    }

    public void setClassFieldName(String classFieldName) {
        this.classFieldName = classFieldName;
    }

    public String getAttributeTableName() {
        return attributeTableName;
    }

    public void setAttributeTableName(String attributeTableName) {
        this.attributeTableName = attributeTableName;
    }

    @Override
    public String toString() {
        return "FieldToAttributeTableEntry{" +
                "classFieldName='" + classFieldName + '\'' +
                ", attributeTableName='" + attributeTableName + '\'' +
                '}';
    }
}
