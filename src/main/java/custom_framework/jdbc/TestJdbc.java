package custom_framework.jdbc;

import com.models.Bank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestJdbc {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");

        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "postgres", "postgres");

        PreparedStatement preparedStatement = connection.prepareStatement("select * from banks");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            String name = resultSet.getString("name");


            System.out.println(name);
            Bank bank = new Bank(name);
        }

    }
}

/**
 * jpa -> orm -> { table -> model } (persistence annotation,- сохраняемый)
 */