package com.services;

import com.models.Bank;

import java.util.List;

public interface BankService {

    List<Bank> getAllBanks();

}
