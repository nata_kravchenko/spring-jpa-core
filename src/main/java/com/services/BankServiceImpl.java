package com.services;

import com.dao.BankDao;
import com.models.Bank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service("bankService")
public class BankServiceImpl implements BankService {
    /**
     * Autowire -> type, name
     */
    @Resource
    @Qualifier("bankDao2")
    private BankDao bankDao1; //= new BankDaoImpl();

    @PersistenceContext
    private EntityManager entityManager;
    /**
     *  Resource пробует инжект сделать полю напрямую как при аннотации @Autowired без Qualifier
     */
    public BankServiceImpl() {
    }

//    @Autowired
//    public BankServiceImpl(BankDao bankDao2, BankDao bankDao1) {
//        this.bankDao1 = bankDao1;
//    }

    public List<Bank> getAllBanks() {
        return bankDao1.getAllBanks();
    }

//    public void setBankDao3(BankDao bankDao1) {
//        this.bankDao1 = bankDao1;
//    }

    public static void main(String[] args) {

    }


}
