package com.controllers;

import com.dao.BankDao;
import com.services.BankService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BankController {
    public static void main(String[] args) {
//        BankService bankService = new BankServiceImpl();
//        System.out.println(bankService.getAllBanks());

        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");
//        BankDao bankDao1 = applicationContext.getBean("bankDao", BankDao.class);
//        Object bankDao2 = applicationContext.getBean("bankDao");

        BankService bankService = applicationContext.getBean("bankService", BankService.class);
        System.out.println(bankService.getAllBanks());
//        System.out.println(bankDao1.hashCode());
//        System.out.println(bankDao2.hashCode());

    }
}
