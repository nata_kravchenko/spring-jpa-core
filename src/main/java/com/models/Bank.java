package com.models;

import custom_framework.annotations.CustomColumn;
import custom_framework.annotations.CustomEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

/**
 * jpa -> jdbc -> url -> db
 * <p>
 * когда идет анализ Entity, Column, OneToMany... diff kind of persistence annotations
 * <p>
 * Bank as class -> bank as table
 * ...
 * name in class level -> bank_name in table level
 */

//@Entity
@CustomEntity(name = "banks")
public class Bank {
    @Column(name = "bank_name")
    @CustomColumn(name = "name")
    private String name;
    @CustomColumn(name = "address")
    private String address;
    //private User manager;

    public Bank(String name) {
        this.name = name;
    }

    public Bank() {
    }

    public Bank(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return Objects.equals(name, bank.name) &&
                Objects.equals(address, bank.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address);
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
