package com.models;

import custom_framework.annotations.CustomColumn;
import custom_framework.annotations.CustomEntity;

import java.util.Objects;

/**
 * jpa -> jdbc -> url -> db
 * <p>
 * когда идет анализ Entity, Column, OneToMany... diff kind of persistence annotations
 * <p>
 * Bank as class -> bank as table
 * ...
 * name in class level -> bank_name in table level
 */


@CustomEntity(name = "company")
public class Company {

    @CustomColumn(name = "id")
    private String id;

    @CustomColumn(name = "name")
    private String name;

    @CustomColumn(name = "source_id")
    private String sourceId;

    private String address;
    //private User manager;

    public Company(String name) {
        this.name = name;
    }

    public Company() {
    }

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company bank = (Company) o;
        return Objects.equals(name, bank.name) &&
                Objects.equals(address, bank.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address);
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
