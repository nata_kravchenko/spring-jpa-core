package com.dao;

import com.models.Bank;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static javax.persistence.PersistenceContextType.EXTENDED;

@Component("bankDao1")
public class BankDaoImpl implements BankDao {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @PersistenceContext(type = EXTENDED)
    private EntityManager entityManager;

    @Transactional
    public List<Bank> getAllBanks() {//HQL
        return entityManager.createQuery("select b from BanksEntity b").getResultList();
    }
}
