package com.dao;

import com.models.Bank;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("bankDao2")
public class BankDaoImpl2 implements BankDao {

    public List<Bank> getAllBanks() {
        return Arrays.asList(new Bank("Bank of USA1", "test3"), new Bank("Bank of USA2", "test4"));
    }
}
