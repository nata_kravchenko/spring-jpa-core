package com.dao;

import com.models.Bank;

import java.util.List;

public interface BankDao {

    List<Bank> getAllBanks();

}
